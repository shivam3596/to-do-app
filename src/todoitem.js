var React = require('react');
var createReactClass = require('create-react-class');
require('./css/todoitem.css');

var TodoItemComponent = createReactClass({
  render:function(){
    return(
      <li>
        <div className="todo-item">
          <div className="item-name">{this.props.item}
          <span className="item-delete" onClick={this.handleDelete}> X</span>
          </div>
        </div>
      </li>
    );
  },
  handleDelete:function(){
    this.props.onDelete(this.props.item);
  }
});

module.exports = TodoItemComponent;
