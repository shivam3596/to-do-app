var React = require('react');
var ReactDOM = require('react-dom');
var TodoItemComponent = require('./todoitem');
var AddItemComponent = require('./additem');
var AboutComponent = require('./about');
var createReactClass = require('create-react-class');
require('./css/index.css');
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

var App = createReactClass({
  render:function(){
    return(
      <Router>
        <Switch>
          <Route exact path={"/"} component={TodoComponent}/>
          <Route path={"/about"} component={AboutComponent}/>
        </Switch>
      </Router>
    );
  }
});

var TodoComponent = createReactClass({
  getInitialState: function(){
    return{
      todos:['eating breakfast','eating lunch','eating dinner'],
      heading:'This is a To do List'
    }
  },
  render:function(){
    var todos = this.state.todos;
    todos = todos.map(function(item,index){
      return(
          <TodoItemComponent item={item} key={index} onDelete={this.onDelete}/>
      );
    }.bind(this));

    return(
      <div>
      <p><a href="/about">{this.state.heading}</a></p>
        <ul>{todos}</ul>
        <AddItemComponent onAdd={this.onAdd}/>
      </div>
    );
  },
  onDelete:function(item){
    var newTodo = this.state.todos.filter(function(val,index){
      return item !== val;
    });
    this.setState({
      todos:newTodo
    });
  },
  onAdd:function(item){
    var newTodos = this.state.todos;
    newTodos.push(item);
    this.setState({
      todos:newTodos
    });
  }
});

ReactDOM.render(<App/>,document.getElementById('todo-wrapper'));
