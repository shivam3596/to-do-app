var React = require('react');
var createReactClass = require('create-react-class');

var AboutComponent = createReactClass({
  render:function(){
    return(
      <h1>This is all about us:)<a href="/"> Back to Home</a></h1>
    );
  }
});

module.exports = AboutComponent;
